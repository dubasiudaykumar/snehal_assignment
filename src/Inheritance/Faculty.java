package Inheritance;

public class Faculty extends Employee{
	
	double AnnualSalary;
	int WeeksPerYear;
	String Department;

	public Faculty(String Name, String Number, String Working,String AnnualSalary, String WeeksPerYear, String Department) {
		super(Name, Number, Working);
		this.AnnualSalary=Double.parseDouble(AnnualSalary);
		this.WeeksPerYear=Integer.parseInt(WeeksPerYear);
		this.Department=Department;		
	}

	public String toString() {
		return employeename + "\t" + employeeid + "\t" + isWorking + "\t" + AnnualSalary
				+ "\t" + WeeksPerYear + "\t" + Department;
	}
	
	@Override
	public double getPay() {
		if(isWorking) {
			AnnualSalary=(AnnualSalary/WeeksPerYear)*2;
			AnnualSalary=Math.round(AnnualSalary);
			}
		return AnnualSalary;
	}

}
