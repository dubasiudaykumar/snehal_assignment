package Inheritance;

public class StudentEmployee extends Employee{

	int hoursWorked;
	double payRate;
	String isworkStudy;

	public StudentEmployee(String Name, String Number, String Working, String Hours,String IsworkStudy,String PayRate) {
		super(Name, Number, Working);
		this.hoursWorked=Integer.parseInt(Hours);
		this.payRate=Double.parseDouble(PayRate);
		this.isworkStudy=IsworkStudy;
	}

	public String toString() {
		return employeename + "\t" + employeeid + "\t" + isWorking + "\t" + this.hoursWorked
				+ "\t" + this.isworkStudy + "\t" + this.payRate;
	}
	
	@Override
	public double getPay() {
		if(isWorking) {
			payRate=(hoursWorked*payRate);
			payRate=Math.round(payRate);
	}
		return payRate;
	}

}
