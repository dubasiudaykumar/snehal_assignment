package Inheritance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
 public static void main(String[] args) throws IOException {
	
//Employee Array
	Employee[] workers=new Employee[11];
	
	String[] str=null;
	
	String filename="C:\\Users\\sneha\\Desktop\\allemployees.csv";
	File file=new File(filename);
	
	int i=0;
	String line;
//Reading csv file
	BufferedReader br =new BufferedReader(new FileReader(file));

//Read every line of csv file  
	while((line=br.readLine())!= null) {
		str=line.split(",");
		if(str.length==5){
			workers[i]=new ClassifiedStaff(str[0],str[1],str[2],str[3],str[4]);
		}
		else if(str.length==6 && (str[4].equals("TRUE")||str[4].equals("FALSE"))) {
			workers[i]=new StudentEmployee(str[0],str[1],str[2],str[3],str[4],str[5]);
		}
		else {
			workers[i]=new Faculty(str[0],str[1],str[2],str[3],str[4],str[5]);
		}
	
		i++;
	}
	br.close();
	for (int j = 0; j < workers.length; j++) {
		System.out.println(workers[j]);
	}

	System.out.println("\t Pay for two week period");
	System.out.println("=================================");
	
//Print pay for two week period
	for (int j = 0; j < workers.length; j++) {
		if(workers[j].isWorking()) {
		System.out.println(workers[j].getEmployeename()+"\t\t\t$"+workers[j].getPay());
		}
	}
}

}
