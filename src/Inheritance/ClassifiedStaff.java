package Inheritance;

public class ClassifiedStaff extends Employee{
	
	String division;
	double weeklySalary; 

	public ClassifiedStaff(String Name, String Number, String Working, String weeklySalary,String division ) {
		super(Name, Number, Working);
		this.weeklySalary=Double.parseDouble(weeklySalary);
		this.division=division;
		
	}
	
	public String toString() {
		return employeename + "\t" + employeeid + "\t"
				+ isWorking + "\t" + weeklySalary + "\t" + division;
	}

	@Override
	public double getPay() {
		if(isWorking) {
			weeklySalary=weeklySalary*2;
			}
		return weeklySalary;
	}

}
