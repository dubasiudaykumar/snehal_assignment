package Assignment8;

public class Student {

	private String name;
	 private String email;
	 private String dob;
	 
	 public Student(String name, String email, String dob) {
		// TODO Auto-generated constructor stub
		 this.name = name;
		 this.email = email;
		 this.dob = dob;
	 }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return name + "," + dob +"," +email;
	} 
	 }
