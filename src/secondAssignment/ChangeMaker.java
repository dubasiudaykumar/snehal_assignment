package secondAssignment;

import java.util.Scanner;

public class ChangeMaker {

	public static void main(String[] args) {
		 Scanner payment = new Scanner(System.in); 
		 System.out.println("Enter the amount the customer paid");
		 int paid = payment.nextInt();
		 
		 Scanner back = new Scanner(System.in); 
		 System.out.println("Enter the amount the customer owed");
		 int owed = back.nextInt();
		 
		 int change = paid-owed;
		 
		 int dollars= change/100;
		 int remain1 = change%100;
		 
		 int quarters = remain1/25;
		 int remain2 = remain1%25;
		 
		 int dimes = remain2/10;
		 int remain3 = remain2 %10;
		 
		 int nickles = remain3 /5;
		 int remain4 = remain3%5;
		 
		 int pennies =remain4 /1;
		 
		 System.out.println("Their change is" +change);
		 System.out.println(dollars+"dollars, "+quarters+"quarters, "+dimes+"dimes, "+nickles+"nickles, 555, and "+pennies+"pennies");
	}
}
