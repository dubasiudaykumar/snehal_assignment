package secondAssignment.RetailStore;

public class Item {

		String name;
		double price;
		int quantity;
		
		public Item (String itemname, double itemprice, int itemquantity) {
			name=itemname;
			price=itemprice;
			quantity= itemquantity;				
		}
		public String getName() {
			return name;
			
		}
		public double getPrice() {
			return price;
			
		}
		public int getQuantity() {
			return quantity;
			
		}
		public double getValue() {
			double value=quantity * price;
			return value;	
		}
	}

