package secondAssignment.RetailStore;

public class Inventory {

	public static void main(String[] args) {
		
		Item item1 = new Item("Stapler", 2.25, 15);
		Item item2 = new Item("Paper", 32.99, 255);
		Item item3=new Item("binder", 4.75, 9);
		double inventory = (item1.getValue()+item2.getValue()+item3.getValue());
		System.out.println("Name\tPrice\tQuantity\tValue");
		
		String name[]= {item1.getName(),item2.getName(),item3.getName()};
		double price[]= {item1.getPrice(),item2.getPrice(),item3.getPrice()};
		int quantity[]= {item1.getQuantity(),item2.getQuantity(),item3.getQuantity()};
		double value[]= {item1.getValue(),item2.getValue(),item3.getValue()};
		
		for(int counter=0;counter<name.length;counter++) {
		System.out.println(name[counter]+"\t"+"$"+price[counter]+"\t"+quantity[counter]+"\t"+"\t"+"$"+value[counter]);
		}
		
		System.out.println("Total inventory is "+"$"+inventory);
		
	}

}
