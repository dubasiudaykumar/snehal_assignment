import java.util.Scanner;

public class EndTime {

	public static void main(String[] args) {
		
		   int startingtimehour;
		   int startingtimemin;
		   int duration;
		   
		   Scanner keyboard = new Scanner(System.in);
		   System.out.println("Enter the starting time(in hours and minutes):");
		   startingtimehour=keyboard.nextInt();
		   startingtimemin=keyboard.nextInt();
		   
		   System.out.println("Enter the duration (in minutes):");
		   duration=keyboard.nextInt();
		   
		   int endingtime=60*(startingtimehour)+startingtimemin+duration;
		   int endinghour= endingtime/60;
		   int endingmin=endingtime%60;
		   
		   System.out.println("Ending hour is:"+endinghour);
		   System.out.println("Ending min is:"+endingmin);
	}

}
