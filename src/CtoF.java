import java.util.Scanner;

public class CtoF {
		 public static void main (String[] args)
		 {

		   double CELS = 37;
		   double FAHR ;
		   System.out.println("This program converts Celsius to Fahrenheit");
		  
		   // Formula used to convert C to F
		   Scanner temp = new Scanner(System.in);
		   System.out.print("Enter temperature in Fahrenheit:");
		   CELS = temp.nextDouble(); 
	       FAHR=CELS*1.8+32;
	   
		   System.out.println("Celsius Temp = " + CELS);
		   System.out.println("Fahrenheit Temp = " + FAHR);

		   System.out.println("End of program");

	}

}
