import java.util.Scanner;

public class Pay {

	public static void main(String[] args) {

		  double hoursWorked;      
	      double hourlyPayRate;
	      double grossPay;
	      
	      Scanner hr = new Scanner(System.in);
	      System.out.print("How many hours did you work? ");
	      hoursWorked= hr.nextDouble();
	      
	      Scanner rate = new Scanner(System.in);
	      System.out.print("How much do you get paid per hour? ");
	      hourlyPayRate= rate.nextDouble();
	      
	      if (hoursWorked <= 40) {
	    	  grossPay=hoursWorked*hourlyPayRate;
	    	  System.out.println("You earned $" + grossPay);
	    	  double tax= 0.33*grossPay;
	    	  double netPay= grossPay-tax;
	    	  System.out.println("You net pay is $" + netPay);
	      }
	      else if (hoursWorked > 40) {
	    	  grossPay= hoursWorked * 1.5 * hourlyPayRate;
	    	  System.out.println("You earned $" + grossPay);
	    	  double tax= 0.33*grossPay;
	    	  double netPay= grossPay-tax;
	    	  System.out.println("You net pay is $" + netPay);	
		}

	}

}
