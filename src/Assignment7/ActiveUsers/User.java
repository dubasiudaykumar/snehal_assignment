package Assignment7.ActiveUsers;

public class User implements Comparable<User> {
	private String name;
    private int times;
    public User(String name, int times) {
   	this.name=name;
   	this.times=times;
    }
    public String getName() {
   	 return name;
    }
    public void setName(String name) {
   	 this.name = name;
    }
    public int getTimes() {
   	 return times;
    }
    public void setTimes(int times) {
   	 this.times=times;
    }

	@Override
	public int compareTo(User o) {
		// TODO Auto-generated method stub
		int compare=Integer.compare(times, o.times);
		return compare;
	}
	
	public String toString() {
		return "{" + name  +","+' ' + times+ ' ' +"times" +"}";
	}

}
