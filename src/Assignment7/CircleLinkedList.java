package Assignment7;

public class CircleLinkedList {

	static class Node 
	{ 
	    int data; 
	    Node next; 
	} 
	static boolean isCircular( Node head) 
	{ 
	   
	    if (head != null)  // change the condition to == for output No
	    return true; 
	  
	    
	    Node node = head.next; 
	  
	    while (node != null && node != head) 
	    node = node.next; 
	  
	     
	    return (node == head); 
	} 
	static Node newNode(int data) 
	{ 
	    Node temp = new Node(); 
	    temp.data = data; 
	    temp.next = null; 
	    return temp; 
	} 
	public static void main(String args[]) 
	{ 
	   
	   Node head = newNode(1); 
	   head.next = newNode(2); 
	   head.next.next = newNode(3); 
	   head.next.next.next = newNode(4); 
	   head.next.next.next.next=newNode(5);
	  
	    System.out.print(isCircular(head)? "Yes\n" : "No\n" ); 	 
	} 
	} 
