package Assignment7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class DictionaryOrder {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>(Arrays.asList("A","Meghana","abc","123","4 Serv","Abcd"));
		System.out.println("The Entered values are " + list);
		
		Collections.sort(list);
		System.out.println("Dictionary order of list is "+ list);
	}
}
		// The alternative way //
//		ArrayList<String> a=new ArrayList<String>();
//	       a.add("A");
//	       a.add("Mahesh");
//	       a.add("abc");
//		   a.add("123");
//	       a.add("4 serv");
//	       a.add("Abcd");
//	       System.out.println("Before soting elements are:");
//	       System.out.println(a);
//	       Collections.sort(a,new MyComparator());
//	       System.out.println("After sorting elements are:");
//	       System.out.println(a);
//	}
//
//}
//class MyComparator implements Comparator<String>
//{
//
//   @Override
//   public int compare(String o1, String o2)
//   {
//      
//       return o1.compareTo(o2);
//      
//   }
//  
//
//}

