package Assignment7;

public class MiddleLinkedList {
	Node head; 	
	  static class Node 
	    { 
	        int value; 
	        Node next; 
	        
	        Node(int value) 
	        { 
	           this.value = value;           
	        } 
	    }    
	   public void addToLast(Node node) 
	    { 
	        
	        if (head == null) {
	        head=node;
	        }else {
	        	Node temp=head;
	        	while(temp.next != null)
	             temp=temp.next;
	        	temp.next=node;
	        }
	        }
	    public void printList() {
	    	Node temp=head;
	    	while(temp != null) {
	    		System.out.println(temp.value);
	    		temp=temp.next;
	    	}
	    	System.out.println();
	    }
	    
	    public Node findMiddleNode(Node head)
	    
	    { 
	      Node slowPointer, fastPointer;
	      slowPointer=fastPointer=head;
	      
	      while(fastPointer != null) {
	    	  fastPointer = fastPointer.next; 
				if(fastPointer != null && fastPointer.next != null) { 
					slowPointer = slowPointer.next; 
					fastPointer = fastPointer.next; 
	      }
	        
	    } 
	  return slowPointer;
	   
	    }
	    public static void main(String [] args) 
	    { 
	    	
	    	MiddleLinkedList list = new MiddleLinkedList();
	    	Node head=new Node(55);
			list.addToLast(head);
			list.addToLast(new Node(7));
			list.addToLast(new Node(100));
			list.addToLast(new Node(1));
			list.addToLast(new Node(200));
	 
			list.printList();
			// finding middle element
			Node middle= list.findMiddleNode(head);
			System.out.println("Middle node will be: "+ middle.value);

		}
	 
	}
	 
