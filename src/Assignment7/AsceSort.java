package Assignment7;

import java.util.ArrayList;
import java.util.Collections;

public class AsceSort {

	public static void main(String[] args) {
		ArrayList<Integer> myNumbers = new ArrayList<Integer>();
	      myNumbers.add(1);
	      myNumbers.add(9);
	      myNumbers.add(6);
	      myNumbers.add(3);
	      myNumbers.add(2);
	      myNumbers.add(8);
	      myNumbers.add(2);
	      myNumbers.add(8);
	      System.out.println("Numbers: "+ myNumbers);
	      Collections.sort(myNumbers);
	      System.out.println("Numbers in ascending order: "+ myNumbers);
	   }

}
