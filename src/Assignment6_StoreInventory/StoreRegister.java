package Assignment6_StoreInventory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class StoreRegister {
	Map<String,Items> inventory= new HashMap<>();

public void loadInventory(File inventoryFile){
	Scanner sc = null;
	try {
		sc = new Scanner(new FileReader(inventoryFile));
	} 
	catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	while(sc.hasNext()){
		String str[]=sc.nextLine().split(",");
		Items part=new Items(str[0], Double.parseDouble(str[1]),str[2]);
		inventory.put(str[0], part);
	}
}

public Receipt checkOutOrder(List<String>items){
	List<Items> ordredItemList=new ArrayList<>();
	for(int j=0;j<items.size();j++){
		Items i=inventory.get(items.get(j));
		try{
			if(i!=null){
			ordredItemList.add(i);
			}
		}catch(NullPointerException exc){
			System.out.println("Given"+items.get(j)+"is not avail");
		}
		
	}
	
	Receipt r=new Billreceipt(ordredItemList);
	return r;
}
}

