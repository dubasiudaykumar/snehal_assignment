package Assignment6_StoreInventory;

import java.util.List;

public interface Receipt {

public String getFormattedTotal();
public List<String> getOrderedItems();

}
