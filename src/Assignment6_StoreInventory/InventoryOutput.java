package Assignment6_StoreInventory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InventoryOutput {
	public static void main(String[] args) throws FileNotFoundException
	{
		StoreRegister sr = new StoreRegister();
		File file =new File("C:\\Users\\sneha\\Desktop/alldata.csv");
		List<String> items = new ArrayList<>();
		items.addAll(Arrays.asList("PC1033","PC800","GenericProcessor","GenericMotherboard","GenericMotherboardV2","LCD","Mouse"));
		sr.loadInventory(file);
		Receipt r=sr.checkOutOrder(items);
		System.out.println(r.getFormattedTotal());
		System.out.println(r.getOrderedItems());
	}

}
