package Assignment6_StoreInventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Billreceipt implements Receipt{
	List<Items> orderedList;
	public Billreceipt(List<Items> orderedList) {
		super();
		this.orderedList = orderedList;
	}

	@Override
	public String getFormattedTotal() {
		double sum =0.0;
		
		for(Items i:orderedList){
			sum=sum+i.price;
			
		}
		String formattedprice=Double.toString(sum);
		return "$"+formattedprice;
	}

	@Override
	public List<String> getOrderedItems() {
		List<String> itmName= new ArrayList<>();
		for(Items i:orderedList){
			itmName.add(i.name);
		}
			Collections.sort(itmName);
		Set<String> ItmNameSo=new LinkedHashSet<>(itmName);
		List<String> itmName1=new ArrayList<>(ItmNameSo);
		return itmName1;
		}
	}


