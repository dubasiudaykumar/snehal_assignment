package Assignment11_SerializationIO.FileCopy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileCopy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			String input = "C:\\Users\\sneha\\Desktop\\text1.txt";
			String output = "C:\\Users\\sneha\\Desktop\\text2.txt";
			/*FileInputStream fis = new FileInputStream(input); 
			FileOutputStream fos = new FileOutputStream(output); 	  
			int b; 
			while  ((b=fis.read()) != -1) 
		    	fos.write(b);
		     fis.close(); 
		     fos.close(); 
	        */
			Path source = Paths.get(input);
			Path destination = Paths.get(output);
			Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
		     File file = new File(output); 
		     BufferedReader br = new BufferedReader(new FileReader(file));
		     String line; 
			 while ((line = br.readLine()) != null) 
			    System.out.println(line); 
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
