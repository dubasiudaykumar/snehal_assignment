package Assignment11_SerializationIO.FindAllFiles;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class FindAllFiles {

	static List<String> fileNames = new ArrayList<String>();
	static List<String[]> fileName = new ArrayList<String[]>();
	public static void main(String[] args) throws IOException {		
		listFiles("C:\\Users\\sneha\\Desktop");
		printFiles();
	}
	private static void printFiles() {
		// TODO Auto-generated method stub
		Collections.sort(fileNames, new java.util.Comparator<String>() {
		    @Override
		    public int compare(String s1, String s2) {
		    	return s1.replaceAll(Pattern.quote("\\"), "\\\\").split("\\\\").length - s2.replaceAll(Pattern.quote("\\"), "\\\\").split("\\\\").length ;
		    }
		});
		
		for(String file: fileNames) {
			System.out.println(file);
		}
		
	}
	private static void listFiles(String path) {
		// TODO Auto-generated method stub
		File folder = new File(path);
        File[] files = folder.listFiles();       
        for (File file : files)
        {
            if (file.isFile() && file.getName().endsWith(".txt"))
            {
            	fileNames.add(file.getAbsolutePath());
            }
            else if (file.isDirectory())
            {
                listFiles(file.getAbsolutePath());
            }
        }
    }	
	}
	

