package Assignment11_SerializationIO.FolderReplicate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

public class FolderReplicate {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		File sourceFolder = new File("C:\\Users\\sneha\\Desktop\\replicate");
        File destinationFolder = new File("C:\\Users\\sneha\\Desktop\\replicate new");
        replicateFolder(sourceFolder, destinationFolder);

	}

	private static void replicateFolder(File sourceFolder, File destinationFolder) throws IOException{
		// TODO Auto-generated method stub
		if (sourceFolder.isDirectory()) 
        {
            //Verify if destinationFolder is already present; If not then create it
            if (!destinationFolder.exists()) 
            {
                destinationFolder.mkdir();
                System.out.println("Directory created :: " + destinationFolder);
            }
      
            String files[] = sourceFolder.list();
            System.out.println(Arrays.toString(files));

            for (String file : files) 
            {
            	//File(File parent, String child) constructor 
            	File srcFile = new File(sourceFolder, file);
                File destFile = new File(destinationFolder, file);
                //Recursive function call
                replicateFolder(srcFile, destFile);
            }
        }
        else
        {
            Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
            System.out.println("File copied :: " + destinationFolder);
        }
		
	}

}
