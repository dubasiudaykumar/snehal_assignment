package Assignment11_SerializationIO.Serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialize {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student student = new Student(12, "Raja K", "raja@gmail.com");
	    String filename = "studentfile.ser"; 
	    
	    // Serialization  
        try
        {    
            //Saving of object in a file 
            FileOutputStream file = new FileOutputStream(filename); 
            ObjectOutputStream out = new ObjectOutputStream(file); 
            
            // Method for serialization of object 
            out.writeObject(student); 	              
            out.close(); 
            file.close(); 
            System.out.println("Student Object has been serialized"); 	            
        } 	          
        catch(IOException ex) 
        { 
            System.out.println(ex); 
        } 
    
        Student student1 = null; 
        
     // Deserialization 
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(filename); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            // Method for deserialization of object 
            student1 = (Student)in.readObject(); 
              
            in.close(); 
            file.close(); 
              
            System.out.println("Student Object has been deserialized "); 
            System.out.println("Id = " + student1.studentId); 
            System.out.println("Name = " + student1.name); 
            System.out.println("Email = " + student1.emailId); 
        } 
          
        catch(Exception ex) 
        { 
            System.out.println(ex); 
        } 
	}

}
