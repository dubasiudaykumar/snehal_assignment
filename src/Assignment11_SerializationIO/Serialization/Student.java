package Assignment11_SerializationIO.Serialization;

import java.io.Serializable;

public class Student implements Serializable{
	public int studentId;
	public String name;
	public String emailId;

	public Student(int studentId, String name, String emailId) {
		// TODO Auto-generated constructor stub
	
		super();
		this.studentId = studentId;
		this.name = name;
		this.emailId = emailId;
	}

}
